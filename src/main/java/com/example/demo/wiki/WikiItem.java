package com.example.demo.wiki;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@JsonIgnoreProperties(ignoreUnknown = true)
@Value
public class WikiItem {
    final private long pageId;
    final private String title;

    @JsonCreator
    public WikiItem(@JsonProperty("pageid") long pageId,
                    @JsonProperty("title") String title) {
        this.pageId = pageId;
        this.title = title;
    }
}
