package com.example.demo.wiki;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@JsonIgnoreProperties(ignoreUnknown = true)
@Value
public class WikiResponse {
    final private WikiResponseQuery query;
    final private WikiResponseError error;

    @JsonCreator
    public WikiResponse(@JsonProperty("query") WikiResponseQuery query, @JsonProperty("error") WikiResponseError error) {
        this.query = query;
        this.error = error;
    }

    public boolean isErrorResponse() {
        return error != null;
    }
}
